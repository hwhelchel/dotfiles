autoload -U compinit
compinit

export CLICOLOR=1
export LSCOLORS=cxfxcxdxbxegedabagacad
export RSENSE_HOME=~/.emacs.d/vendor/rsense-0.3/bin/rsense
export LESS=-r

source ~/.prompt
if [[ -f ~/.prompt.local ]]; then source ~/.prompt.local; fi
source ~/.aliases
source ~/.exports
# source ~/work/ove_ruby_tools/github/iam_init.sh

# Rake completions
_rake () {
  if [ -f Rakefile ]; then
    compadd `rake --silent --tasks | cut -d " " -f 2`
  fi
}
compdef _rake rake

# Cap completions
_cap () {
  if [ -f Capfile ]; then
    compadd `cap --tasks --verbose | grep '^cap ' | cut -d " " -f 2`
  fi
}
compdef _cap cap

# History options
setopt APPEND_HISTORY
setopt CORRECT     # command CORRECTION
setopt EXTENDED_HISTORY   # puts timestamps in the history

# number of lines kept in history
export HISTSIZE=10000

# number of lines saved in the history after logout
export SAVEHIST=10000

# location of history
export HISTFILE=~/.zhistory

# history options
setopt inc_append_history
setopt hist_ignore_all_dups
setopt hist_ignore_dups
setopt hist_allow_clobber
setopt hist_reduce_blanks

bindkey -e
bindkey "^[[1;2C" emacs-forward-word
bindkey "^[[1;2D" emacs-backward-word
bindkey "^[[3~" delete-char

export MAVEN_OPTS="-Xmx1024m -XX:MaxPermSize=256m"

# Base16 Shell

BASE16_SCHEME="default"
BASE16_SHELL="$HOME/.config/base16-shell/base16-$BASE16_SCHEME.dark.sh"
[[ -s $BASE16_SHELL ]] && . $BASE16_SHELL

eval "$(rbenv init -)"
