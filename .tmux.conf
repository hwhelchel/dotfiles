set -g default-terminal "xterm-256color"

# Set prefix command to Control-\
unbind-key C-b
set-option -g prefix 'C-\'

# C-\ C-\ jumps to the last open window
bind-key 'C-\' last-window

# C-\ C-\ cycles among panes
# bind C-\ select-pane -t :.+

# count new windows from 1
set -g base-index 1

# Set status bar
set -g status-bg black
set -g status-fg white
set -g status-left "#[fg=blue]#S"
set -g status-left ""
set -g status-right "#[fg=green]#H #(date +'%a, %d %b %Y, %R')"

# Highlight active window
set-window-option -g window-status-current-bg colour239

# vi mode
bind-key -r j select-pane -U
bind-key -r k select-pane -D
bind-key -r h select-pane -L
bind-key -r l select-pane -R

# shell magic to distinguish C-arrow keys for pane commands
# does not work
#set -g terminal-overrides "*:kUP5=\eOA,*:kDN5=\eOB,*:kLFT5=\eOD,*:kRIT5=\eOC"

# ssh interactive
bind-key S command-prompt -p "ssh: " "new-window -n %1 'ssh %1'"
set -g history-limit 10000

# opens htop in a split pane
bind-key H split-window -h 'htop'

# binding for horizontal split
bind-key _ split

# bind key for synchronizing-panes
bind-key y set-window-option synchronize-panes

# ease timeout for exiting vim's insert mode
set -sg escape-time 0

# toggle mouse with leader m
# mouse can be used to select panes, windows, resize, etc
bind m \
  set-option -g mode-mouse on \;\
  set-option -g mouse-select-pane on \;\
  set-option -g mouse-select-window on \;\
  set-option -g mouse-resize-pane on \;\
  display 'Mouse mode ENGAGED'

bind M \
  set-option -g mode-mouse off \;\
  set-option -g mouse-select-pane off \;\
  set-option -g mouse-select-window off \;\
  set-option -g mouse-resize-pane off \;\
  display 'Mouse mode DISENGAGED'

# mouse scrolling
setw -g mode-mouse on

